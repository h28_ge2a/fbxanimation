#pragma once

//インクルード
#include <Windows.h>
#include <d3dx9.h>
#include "MyRect.h"
#include "Input.h"

//開放用マクロ
#define SAFE_DELETE(p) if(p){delete p; p = nullptr;} 
#define SAFE_DELETE_ARRAY(p) if(p){delete[] p; p = nullptr;} 
#define SAFE_RELEASE(p) if(p){p->Release() ; p = nullptr;} 

class Scene;

//グローバル変数
class Global
{
public:
	//定数
	const int	WINDOW_WIDTH = 800;		//ウィンドウの幅
	const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

	//変数
	LPDIRECT3DDEVICE9	pDevice;	     //Direct3Dデバイスオブジェクト
	Scene* pScene;
	Input *pInput;

	//関数
	void replaceScene(Scene* pNextScene);	//シーン切り替え
};

//グローバルオブジェクト（外部宣言）
extern Global g;    //グローバル変数