#pragma once

#include <dinput.h>
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")

class Input
{
	LPDIRECTINPUT8			_pDInput;	//DirectInput本体

	LPDIRECTINPUTDEVICE8	_pKeyDevice;//キーボード
	BYTE _keyState[256];		//キーの情報
	BYTE _prevKeyState[256];	//前回のキーの情報

public:
	Input();
	~Input();
	void init(HWND hWnd);
	void Input::update();
	BOOL Input::isKeyPush(int keyCode);		//キーが押されているか
	BOOL Input::isKeyTap(int keyCode);		//キーを押した瞬間か
	BOOL Input::isKeyRelease(int keyCode);	//キーを離した瞬間か
};

