#include "Quad.h"


Quad::Quad()
{
	_vertexBuffer = nullptr;
	_indexBuffer = nullptr;
	_pTexture = nullptr;
}


Quad::~Quad()
{
	SAFE_RELEASE(_vertexBuffer);
	SAFE_RELEASE(_indexBuffer);
	SAFE_RELEASE(_pTexture);
}


Quad* Quad::create(LPCSTR fileName)
{
	auto quad = new Quad();
	quad->load(fileName);
	return quad;
}

void Quad::load(LPCSTR fileName)
{
	//頂点情報×４
	Vertex vertexList[] = {
		D3DXVECTOR3(-1, 1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0),
		D3DXVECTOR3(1, 1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0),
		D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1)
		
	};


	//バーテックスバッファ
	g.pDevice->CreateVertexBuffer(sizeof(vertexList), 0, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &_vertexBuffer, 0);
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(vertexList));
	_vertexBuffer->Unlock();


	//インデックスバッファ
	int indexList[] = { 0, 2, 3, 0, 1, 2 };
	g.pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &_indexBuffer, 0);
	DWORD *iCopy;
	_indexBuffer->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	_indexBuffer->Unlock();


	//マテリアル
	ZeroMemory(&_material, sizeof(D3DMATERIAL9));
	_material.Diffuse.r = 1.0f;
	_material.Diffuse.g = 1.0f;
	_material.Diffuse.b = 1.0f;
	_material.Diffuse.a = 1.0f;

	//テクスチャ
	if (strlen(fileName) != 0)
		D3DXCreateTextureFromFileEx(g.pDevice, fileName, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture);
}

void Quad::draw()
{
	g.pDevice->SetStreamSource(0, _vertexBuffer, 0, sizeof(Vertex));//バーテックスバッファ
	g.pDevice->SetIndices(_indexBuffer);							//インデックスバッファ
//	g.pDevice->SetMaterial(&_material);		//マテリアル
//	g.pDevice->SetTexture(0, _pTexture);	//テクスチャ
	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);//頂点情報


	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * _billboard * rotateZ * rotateX * rotateY * trans;


	//ワールド行列をセット
	g.pDevice->SetTransform(D3DTS_WORLD, &world);

	//描画
	g.pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
	g.pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

void Quad::setBillboard(D3DXMATRIX billboard)
{
	_billboard = billboard;
}