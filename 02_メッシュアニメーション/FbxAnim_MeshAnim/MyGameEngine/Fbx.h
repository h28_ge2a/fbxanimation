#pragma once
#include "Node.h"
#include <vector>
#include <fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")




//レイキャスト用構造体
struct RayCastData
{
	D3DXVECTOR3 start;//レイ発射位置
	D3DXVECTOR3 dir;  //レイの向きベクトル
	float       dist; //衝突点までの距離
	BOOL        hit;  //レイが当たったか
	D3DXVECTOR3 normal;//法線
};



class Fbx :	public Node
{
	//頂点情報を扱う構造体
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};



	//１パーツごとの情報（本当は別クラスにした方がいいと思う）
	struct FbxParts
	{
		int _vertexCount;				//頂点数
		int _polygonCount;				//ポリゴ数
		int _indexCount;				//インデックス数
		int _materialCount;				//マテリアルの個数
		int* _polygonCountOfMaterial;	//マテリアルごとのポリゴン数

		LPDIRECT3DVERTEXBUFFER9		_vertexBuffer;	//頂点バッファ
		LPDIRECT3DINDEXBUFFER9*		_indexBuffer;	//インデックスバッファ
		D3DMATERIAL9*				_material;		//マテリアル
		LPDIRECT3DTEXTURE9*			_pTexture;		//テクスチャ


		//子供のパーツ
		std::vector<FbxParts*>	childParts;

		//ノードのポインタ
		FbxNode *pNode;

		//コンストラクタ
		FbxParts()
		{
			_vertexCount = 0;
			_polygonCount = 0;
			_indexCount = 0;
			_materialCount = 0;
			_polygonCountOfMaterial = NULL;
			_material = NULL;
			_pTexture = NULL;
			_vertexBuffer = NULL;
			_indexBuffer = NULL;

		}

		//デストラクタ
		~FbxParts()
		{
			for (int i = 0; i < _materialCount; i++)
			{
				SAFE_RELEASE(_indexBuffer[i]);
				SAFE_RELEASE(_pTexture[i]);
			}
			SAFE_DELETE_ARRAY(_indexBuffer);
			SAFE_DELETE_ARRAY(_pTexture);
			SAFE_DELETE_ARRAY(_material);

			SAFE_DELETE_ARRAY(_polygonCountOfMaterial);

			SAFE_RELEASE(_vertexBuffer);

			for (int i = 0; i < childParts.size(); i++)
			{
				SAFE_DELETE(childParts[i]);
			}
			childParts.clear();
		}
	};

	//ロードに必要なやつら
	FbxManager*  _manager;
	FbxImporter* _importer;
	FbxScene*    _scene;

	// アニメーションのフレームレート
	FbxTime::EMode				_frameRate;

	//現在のフレーム
	int	_frame;

	//フレームの最初と最後
	int _startFrame, _endFrame;

	//パーツ（複数あるかも）
	std::vector<FbxParts*>	parts;



	void checkNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList);
	void checkMesh(FbxMesh* pMesh, FbxParts* pParts);
	void draw(FbxParts* pParts);


protected:
	D3DXMATRIX createWorldMatrix();

public:
	Fbx();
	~Fbx();
	static Fbx* create(LPCSTR fileName);
	void load(LPCSTR fileName);
	void draw();
	void rayCast(RayCastData *data);
	void setAnimFrame(int startFrame, int endFrame);
};

