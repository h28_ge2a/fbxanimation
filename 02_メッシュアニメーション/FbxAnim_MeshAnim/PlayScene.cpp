#include "PlayScene.h"

#include "MyGameEngine\Camera.h"
#include "MyGameEngine\Light.h"



PlayScene::PlayScene()
{

}


PlayScene::~PlayScene()
{

}

void PlayScene::init()
{
	_camera->setPosition(0, 5, -10);

	auto light = Light::create();
	this->addChild(light);

	auto fbx = Fbx::create("Assets\\anim.fbx");
	fbx->setAnimFrame(0, 30);					//0〜30フレームの間でアニメーション
	this->addChild(fbx);
}


void PlayScene::update()
{
	Scene::update();



}

void PlayScene::input()
{
	Scene::input();

}