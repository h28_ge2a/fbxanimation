#pragma once
#include "Node.h"


class Quad :public Node
{
	//頂点情報
	struct Vertex 
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線
		D3DXVECTOR2 uv;		//UV座標
	};


	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;		//バーテックスバッファ
	LPDIRECT3DINDEXBUFFER9 _indexBuffer;		//インデックスバッファ
	D3DMATERIAL9         _material;				//マテリアル
	LPDIRECT3DTEXTURE9 _pTexture;				//テクスチャ
	D3DXMATRIX _billboard;

public:
	Quad();
	~Quad();
	static Quad* create(LPCSTR fileName);
	void load(LPCSTR fileName ="");
	void draw();

	void setBillboard(D3DXMATRIX billboard);
};

