#include "Fbx.h"


Fbx::Fbx()
{

}


Fbx::~Fbx()
{
	//各パーツの開放
	for (int i = 0; i < parts.size(); i++)
	{
		SAFE_DELETE(parts[i]);
	}
	parts.clear();

	_manager->Destroy();
}


Fbx* Fbx::create(LPCSTR fileName)
{
	auto fbx = new Fbx();
	fbx->load(fileName);
	return fbx;
}

//FBXファイル読み込み
void Fbx::load(LPCSTR fileName)
{
	//ロードに必要なやつらを準備
	_manager = FbxManager::Create();				//一番えらいやつ
	_importer = FbxImporter::Create(_manager, "");	//ファイルを読み込むやつ
	_scene = FbxScene::Create(_manager, "");		//読み込んだファイルを管理するやつ

	//インポーターを使ってファイルロード
	_importer->Initialize(fileName);
	_importer->Import(_scene);

	//インポーターはお役御免
	_importer->Destroy();

	//現在のカレントディレクトリを覚えておく
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをファイルがあった場所に変更
	char dir[MAX_PATH];
	_splitpath_s(fileName, NULL, 0, dir, MAX_PATH, NULL, 0, NULL, 0);
	SetCurrentDirectory(dir);


	//ルートノードを取得して
	FbxNode* rootNode = _scene->GetRootNode();

	//そいつの子供の数を調べて
	int childCount = rootNode->GetChildCount();

	//1個ずつチェック
	for (int i = 0; childCount > i; i++)
	{
		checkNode(rootNode->GetChild(i), &parts);
	}


	//カレントディレクトリを元の位置に戻す
	SetCurrentDirectory(defaultCurrentDir);
}

//ノードの中身を調べる
void Fbx::checkNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList)
{
	//そのノードにはメッシュ情報が入っているだろうか？
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//パーツ情報を入れる構造体
		FbxParts* pParts = new FbxParts;

		//ノード情報のアドレスを入れておく
		pParts->pNode = pNode;

		//各データを入れる準備
		pParts->_materialCount = pNode->GetMaterialCount();						//マテリアルは何個使ってる？
		pParts->_material = new D3DMATERIAL9[pParts->_materialCount];			//マテリアルを入れる配列作成
		pParts->_pTexture = new LPDIRECT3DTEXTURE9[pParts->_materialCount];		//テクスチャを入れる配列作成


		//マテリアル情報を1個ずつ確認していく
		for (int i = 0; i < pParts->_materialCount; i++)
		{
			//マテリアル情報ゲット
			FbxSurfaceLambert* lambert = (FbxSurfacePhong*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;
			FbxDouble3 ambient = lambert->Ambient;
			
			//用意した構造体に入れていく
			ZeroMemory(&pParts->_material[i], sizeof(D3DMATERIAL9));

			//ポリゴンの色
			pParts->_material[i].Diffuse.r = (float)diffuse[0];
			pParts->_material[i].Diffuse.g = (float)diffuse[1];
			pParts->_material[i].Diffuse.b = (float)diffuse[2];
			pParts->_material[i].Diffuse.a = 1.0f;

			//環境光
			pParts->_material[i].Ambient.r = (float)ambient[0];
			pParts->_material[i].Ambient.g = (float)ambient[1];
			pParts->_material[i].Ambient.b = (float)ambient[2];
			pParts->_material[i].Ambient.a = 1.0f;


			//テクスチャ情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャ使ってない場合
			if (textureFile == NULL)
			{
				pParts->_pTexture[i] = NULL;
			}

			//テクスチャ使ってる場合
			else
			{
				//ファイル名を調べて
				const char* textureFileName = textureFile->GetFileName();

				//ファイル名+拡張だけにする
				char name[_MAX_FNAME];//ファイル名
				char ext[_MAX_EXT];//拡張子
				_splitpath_s(textureFileName, NULL, 0, NULL, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);

				//画像ファイルを読み込んでテクスチャ作成
				D3DXCreateTextureFromFileEx(g.pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
					D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pParts->_pTexture[i]);
			}
		}

		//次は頂点情報を取得するぞ〜
		checkMesh(pNode->GetMesh(), pParts);

		//パーツ情報を動的配列に追加
		pPartsList->push_back(pParts);
	}

	//子ノードにもデータがあるかも！！
	{
		//子供の数を調べて
		int childCount = pNode->GetChildCount();

		//一人ずつチェック
		for (int i = 0; i < childCount; i++)
		{
			checkNode(pNode->GetChild(i), pPartsList);
		}
	}
}


//頂点情報を取得
void Fbx::checkMesh(FbxMesh* pMesh, FbxParts* pParts)
{
	//とりあえず専用のデータ型に入れる
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//各情報を調べる
	pParts->_vertexCount = pMesh->GetControlPointsCount();	//頂点数
	pParts->_polygonCount = pMesh->GetPolygonCount();		//ポリゴン数
	pParts->_indexCount = pMesh->GetPolygonVertexCount();	//インデックス数

	//頂点データを入れる配列作成
	Vertex* vertexList = new Vertex[pParts->_vertexCount];


	/////////////////////////頂点の位置/////////////////////////////////////

	//1個ずつ頂点の位置を入れていく
	for (int i = 0; pParts->_vertexCount > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}


	//ポリゴン1枚ずつ調べていく
	for (int i = 0; i < pParts->_polygonCount; i++)
	{
		//データの先頭をゲットして
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		//3頂点分
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			/////////////////////////頂点の法線/////////////////////////////////////
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);


			/////////////////////////頂点のＵＶ/////////////////////////////////////
			int uvIndex = pMesh->GetTextureUVIndex(i, j, FbxLayerElement::eTextureDiffuse);
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));	//Ｙ座標が上下逆だそうです
		}
	}

	/////////////////////////////////////////////////
	//  ここまでで、各頂点の情報は全部分かった！！！！ 
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	//  頂点バッファを作成！！
	/////////////////////////////////////////////////
	
	//まずカラのバッファ作って
	g.pDevice->CreateVertexBuffer(sizeof(Vertex)*pParts->_vertexCount, 0, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pParts->_vertexBuffer, 0);

	//ロックすればいじれる
	Vertex *vCopy;
	pParts->_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);

	//カラのバッファにデータコピー
	memcpy(vCopy, vertexList, sizeof(Vertex)*pParts->_vertexCount);

	//終わったらアンロック
	pParts->_vertexBuffer->Unlock();
	delete[] vertexList;


	/////////////////////////////////////////////////
	//	頂点バッファ完成！！！！
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	//	こっからインデックス情報をかき集める
	/////////////////////////////////////////////////

	//インデックス数分の配列を作成
	pParts->_indexBuffer = new IDirect3DIndexBuffer9*[pParts->_materialCount];

	//マテリアルごとのポリゴン数を入れるための配列作成
	pParts->_polygonCountOfMaterial = new int[pParts->_materialCount];

	//マテリアルごとにインデックスバッファ作る
	for (int i = 0; i < pParts->_materialCount; i++)
	{
		//とりあえずインデックス番号入れる配列
		int* indexList = new int[pParts->_indexCount];

		//ポリゴンごとに
		int count = 0;
		for (int polygon = 0; polygon < pParts->_polygonCount; polygon++)
		{
			//そのポリゴンのマテリアルの番号をゲット
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			//今処理中のマテリアルだった
			if (materialID == i)
			{
				//3つ分
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//マテリアルごとのポリゴン数　＝　頂点数÷３
		pParts->_polygonCountOfMaterial[i] = count / 3;

		/////////////////////////////////////////////////
		//インデックスバッファ作成！！
		/////////////////////////////////////////////////

		//まずカラのバッファ作って
		g.pDevice->CreateIndexBuffer(sizeof(int)* pParts->_indexCount, 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pParts->_indexBuffer[i], 0);

		//ロックして
		DWORD *iCopy;
		pParts->_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);

		//コピーして
		memcpy(iCopy, indexList, sizeof(int)* pParts->_indexCount);

		//ロック解除
		pParts->_indexBuffer[i]->Unlock();
		delete[] indexList;
	}
}


//ワールド行列を作成
D3DXMATRIX Fbx::createWorldMatrix()
{
	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;

	return world;
}

//描画
void Fbx::draw()
{
	//パーツを1個ずつ描画
	for (int k = 0; k < parts.size(); k++)
	{
		draw( parts[k]);
	}
}


//パーツを描画
//引数：pParts　描画したいパーツ
void Fbx::draw(FbxParts* pParts)
{
	//頂点バッファの設定
	g.pDevice->SetStreamSource(0, pParts->_vertexBuffer, 0, sizeof(Vertex));

	//頂点ストリームを指定
	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//ワールド行列をセット
	D3DXMATRIX world = createWorldMatrix();
	g.pDevice->SetTransform(D3DTS_WORLD, &world);

	//マテリアルごとに描画
	for (int i = 0; i < pParts->_materialCount; i++)
	{
		g.pDevice->SetIndices(pParts->_indexBuffer[i]);
		g.pDevice->SetTexture(0, pParts->_pTexture[i]);
		g.pDevice->SetMaterial(&pParts->_material[i]);
		g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, pParts->_vertexCount, 0, pParts->_polygonCountOfMaterial[i]);
	}
}


//レイキャストもパーツごとにやらなきゃいけない
//今回は面倒なので省略
void Fbx::rayCast(RayCastData *data)
{
	//D3DXMATRIX world = createWorldMatrix();
	//data->hit = FALSE;
	//data->dist = 99999.0f;

	////頂点バッファをロック
	//Vertex *vCopy;
	//_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);

	////マテリアル毎
	//for (DWORD i = 0; i < _materialCount; i++)
	//{
	//	//インデックスバッファをロック
	//	DWORD *iCopy;
	//	_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);

	//	//そのマテリアルのポリゴン毎
	//	for (DWORD j = 0; j < _polygonCountOfMaterial[i]; j++)
	//	{
	//		//3頂点
	//		D3DXVECTOR3 ver[3];
	//		ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
	//		ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
	//		ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

	//		D3DXVec3TransformCoord(&ver[0], &ver[0], &world);
	//		D3DXVec3TransformCoord(&ver[1], &ver[1], &world);
	//		D3DXVec3TransformCoord(&ver[2], &ver[2], &world);

	//		D3DXVECTOR3 v1 = ver[1] - ver[0];
	//		D3DXVECTOR3 v2 = ver[2] - ver[1];
	//		D3DXVec3Cross(&data->normal, &v1, &v2);
	//		D3DXVec3Normalize(&data->normal, &data->normal);


	//		BOOL  hit;
	//		float dist;

	//		hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
	//			&data->start, &data->dir, NULL, NULL, &dist);

	//		if (hit && data->dist > dist)
	//		{
	//			data->hit = TRUE;
	//			data->dist = dist;
	//		}


	//	}






	//	//インデックスバッファ使用終了
	//	_indexBuffer[i]->Unlock();
	//}

	////頂点バッファ使用終了
	//_vertexBuffer->Unlock();
}
