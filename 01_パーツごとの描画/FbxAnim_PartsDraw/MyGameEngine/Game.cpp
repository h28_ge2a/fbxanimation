#include "game.h"
#include "Global.h" 
#include "Sprite.h"
#include "..\PlayScene.h"


//コンストラクタ
Game::Game()
{
	_pD3d = nullptr;
	g.pScene = nullptr;
	g.pInput = nullptr;
	
}

//デストラクタ
Game::~Game()
{
	SAFE_DELETE(g.pInput);
	SAFE_DELETE (g.pScene);

	SAFE_RELEASE(g.pDevice);
	SAFE_RELEASE(_pD3d);
}


//初期化処理
void Game::init(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	_pD3d = Direct3DCreate9(D3D_SDK_VERSION); //DirectX本体の準備	

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.WINDOW_WIDTH;
	d3dpp.BackBufferHeight = g.WINDOW_HEIGHT;
	_pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &g.pDevice);


	//最初のシーン
	g.pScene = new PlayScene; 
	g.pScene->init();
	
	//Inputオブジェクト
	g.pInput = new Input;
	g.pInput->init(hWnd);

	//半透明描画あり
	g.pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	g.pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g.pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//更新処理
void Game::update()
{
	g.pScene->update();
}

//入力処理
void Game::input()
{
	g.pInput->update();
	g.pScene->input();
}

//描画処理
void Game::draw()
{
	//画面をクリア
	g.pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 255, 255), 1.0f, 0);

	//描画開始
	g.pDevice->BeginScene();

	//ゲーム画面の描画
	g.pScene->draw();

	//描画終了
	g.pDevice->EndScene();

	//スワップ
	g.pDevice->Present(NULL, NULL, NULL, NULL);
}