#include "Input.h"

//コンストラクタ
Input::Input()
{
	_pDInput = nullptr;
	_pKeyDevice = nullptr;
}

//デストラクタ
Input::~Input()
{
	_pKeyDevice->Release();
	_pDInput->Release();
}

//初期化
void Input::init(HWND hWnd)
{
	//DirectInut本体
	DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&_pDInput, NULL);

	//キーボード準備
	_pDInput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, NULL);	//デバイスオブジェクトの作成
	_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);					//デバイスの種類
	_pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);		//強調レベル
}

//更新
void Input::update()
{
	_pKeyDevice->Acquire();
	memcpy(_prevKeyState, _keyState, sizeof(_keyState));		//前回のキーの状態
	_pKeyDevice->GetDeviceState(sizeof(_keyState), &_keyState); //すべてのキーの状態を取得
	
}


//キーが押されているか
BOOL Input::isKeyPush(int keyCode) 
{
	if (_keyState[keyCode] & 0x80)
	{
		return TRUE;
	}
	return FALSE;
}

//キーを押した瞬間か
BOOL Input::isKeyTap(int keyCode)
{
	if (!(_prevKeyState[keyCode] & 0x80) && _keyState[keyCode] & 0x80)
	{
		return TRUE;
	}
	return FALSE;
}

//キーを離した瞬間か
BOOL Input::isKeyRelease(int keyCode)
{
	if (_prevKeyState[keyCode] & 0x80 && !(_keyState[keyCode] & 0x80))
	{
		return TRUE;
	}
	return FALSE;
}