#include "Label.h"

//初期化
Label::Label()
{
	_pFont = nullptr;
}

//開放処理
Label::~Label()
{
	SAFE_DELETE (_pFont);
}

//作成
Label * Label::create(LPCSTR label, LPCSTR font, int _scale)
{
	Label* pLabel = new Label;
	pLabel->load(font, _scale);
	pLabel->setString(label);
	return pLabel;
}

//フォント作成
void Label::load(LPCSTR font, int size)
{
	D3DXCreateFont(g.pDevice, size, 0, FW_NORMAL, 1,
		FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, "ＭＳ ゴシック", &_pFont);
}

//描画
void  Label::draw()
{
	//表示範囲
	RECT rect;
	rect.left = _position.x - _size.x * _anchorPoint.x;
	rect.top = _position.y - _size.y * _anchorPoint.y;
	rect.right = rect.left + _size.x;
	rect.bottom = rect.top + _size.y;

	//描画
	_pFont->DrawText(NULL, _string, -1, &rect, DT_LEFT | DT_TOP, D3DCOLOR_ARGB(255, 255, 255, 255));
}

//表示する文字列の変更
void Label::setString(LPCSTR str)
{
	strcpy(_string, str);

	//表示に必要な範囲を取得
	RECT rect = { 0, 0, 0, 0 };
	_pFont->DrawText(NULL, _string, -1, &rect, DT_CALCRECT, NULL);

	//サイズを更新
	_size.x = rect.right;
	_size.y = rect.bottom;
}