#include "Camera.h"

//コンストラクタ
Camera::Camera()
{
	_position = D3DXVECTOR3(0, 3, -5);
	_target = D3DXVECTOR3(0, 0, 0);
	_up = D3DXVECTOR3(0, 1, 0);
	_angle = 60;
	_aspect = (float)g.WINDOW_WIDTH / g.WINDOW_HEIGHT;
	_nearClip = 0.3f;
	_farClip = 1000.0f;

	D3DXMatrixIdentity(&_view);
	D3DXMatrixIdentity(&_proj);
}

//デストラクタ
Camera::~Camera()
{
	
}

//作成
Camera* Camera::create()
{
	Camera* pCamera = new Camera();
	return pCamera;
}

//更新
void Camera::update()
{
	//ビュー行列作成
	D3DXMatrixLookAtLH(&_view, &_position, &_target, &_up);
	g.pDevice->SetTransform(D3DTS_VIEW, &_view);
	
	//プロジェクション行列作成
	D3DXMatrixPerspectiveFovLH(&_proj, D3DXToRadian(_angle), _aspect, 
		_nearClip, _farClip);
	g.pDevice->SetTransform(D3DTS_PROJECTION, &_proj);


	//
	D3DXMatrixLookAtLH(&_billboard,
		&D3DXVECTOR3(0, 0, 0),
		&(_target - _position), &_up);

	D3DXMatrixInverse(
		&_billboard,
		NULL,
		&_billboard);

}






//////////各セッター//////////////////
void Camera::setTarget(D3DXVECTOR3 target)
{

	_target = target;
}

void Camera::setUp(D3DXVECTOR3 up)
{
	_up = up;
}

void Camera::setAngle(float angle)
{
	_angle = angle;
}

void Camera::setAspect(float aspect)
{
	_aspect = aspect;
}

void Camera::setNearClip(float nearClip)
{
	_nearClip = nearClip;
}

void Camera::setFarClip(float farClip)
{
	_farClip = farClip;
}

D3DXMATRIX Camera::getBillboard()
{
	return _billboard;
}

D3DXMATRIX Camera::getView()
{
	return _view;
}

D3DXMATRIX Camera::getProj()
{
	return _proj;
}