#pragma once
#include "Global.h"
#include "Sprite.h"
#include "Label.h"
#include "Node.h"
#include "Quad.h"
#include "Fbx.h"
#include <vector>

class Camera;
class Scene
{
protected:
	std::vector<Node*> _nodes;
	Camera* _camera;

public:
	Scene();
	virtual ~Scene();
	void addChild(Node* pNode);
	void draw();
	void removeChild(Node* pNode);

	virtual void init() = 0;
	virtual void update();
	virtual void input();
};

