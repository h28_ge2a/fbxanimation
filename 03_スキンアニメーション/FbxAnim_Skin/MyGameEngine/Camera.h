#pragma once
#include "Node.h"
class Camera :public Node
{
	D3DXVECTOR3 _target;//視点
	D3DXVECTOR3 _up;	//上方向

	float _angle;		//画角
	float _aspect;		//アスペクト比
	float _nearClip;	//近クリッピング
	float _farClip;		//遠クリッピング

	D3DXMATRIX _view;	//ビュー行列
	D3DXMATRIX _proj;	//射影行列
	D3DXMATRIX _billboard;


public:
	Camera();
	~Camera();
	static Camera* create();
	void update();



	//各セッター
	void setTarget(D3DXVECTOR3 target);
	void setUp(D3DXVECTOR3 up);
	void setAngle(float angle);
	void setAspect(float aspect);
	void setNearClip(float nearClip);
	void setFarClip(float farClip);

	D3DXMATRIX getBillboard();
	D3DXMATRIX getView();
	D3DXMATRIX getProj();
};

